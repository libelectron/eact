# Electron Automated Compiling Toolkit

A toolkit for building your electron applications with Electron-packager and electron-build for non DRM and DRM applications and then zip them up ready for you to be distrobuted.

## Documentation

This script requires you to run it inside the electron folder and requires a package.json file so it can install the dependancies of your application. You will need to to install and have electron-packager and electron-builder in your package.json file beforehand. 

Once you have ran eact, eact will create folder a folder outside of the application called temp_output which is where the compiled and packaged applications will be stored.


## All Commands
```
-e    Compiles your application using Electron
-w    Compiles your application using Electron Widevine for DRM content
-c    Cleans application folder by removing package-lock.json, node_modules folder, and dist folder
-i    Install npm package dependencies
-v    Version
```

[Releases](https://gitlab.com/libelectron/eact/-/releases)


 ### Electron Automated Compiling Toolkit is avaliale on the AUR.
  
  [Click here](https://aur.archlinux.org/packages/eact)


&nbsp;&nbsp;&nbsp;&nbsp;

### Author

* Corey Bruce
